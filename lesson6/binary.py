f = open ("wb.dat", mode="wb")

f.write(b"Hello, world!\n")
f.write(bytes((1,2,3,4)))

f.close()
f = open ("wb.dat", mode="rb")

print(f.read())
f.close()

with open("wb.dat", mode="rb") as f:
    print(f.read())