f = open("./rt.txt", mode="rt")

print(f.read()) #прочитать файл
print("-" * 40)

f.close()
f = open("./rt.txt", mode="rt")

print(f.read(8))
print(f.read(6))
print("-" * 40)

f.close()
f = open("./rt.txt", mode="rt")

print(f.readline()) #прочитать одну строчку из файла
print(f.readline())
print("-" * 40)

f.close()
f = open("./rt.txt", mode="rt")

print(f.readlines()) #создает список из строк
print("-" * 40)
f.close()
f = open("./rt.txt", mode="rt")

for line in f:
    print(line) #читает строчку за строчкой
print("-" * 40)
f.close()








