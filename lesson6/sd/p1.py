import pickle # подключаем библиотеку

obj = {"string": 1, True: 2.5, None: [range(20), b"123"]} 
print("obj:", obj)

data = pickle.dumps(obj, protocol=2) #превращаем объект в данные
print("data:", data)

new_obj = pickle.loads (data) #превращаем данные в объект
print("new_obj:", new_obj)