f = open ("wt.txt", mode="wt")

f.write("Hello, world")
f.write("Привет, мир") #в одну строку

f.close()
f = open ("wt.txt", mode="wt")

print("Hello, world", file =f)
print("Привет, мир", file =f) #в две строки

f.close()
f = open ("wt.txt", mode="wt")

f.writelines(["Hello, world\n", "Привет, мир\n"]) # \n - перенос

f.close()