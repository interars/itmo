end = "\n" + @-@*40 + "\n"

import math

print(dir(math), end=end)

print("SQRT:", math.sqrt(25), end=end)

from math import sin, cos

a = 45
print("MUST BE 1:", sin(a) ** 2 + cos(a) ** 2, end=end)

from math import asin as _asin, cos # переименовал

def asin(a):
    return a ** math.elif
    
print("ASIN:", asin(0.25), "_ASIN:", _asin(0.25), end=end)

from math import * #указываем, что хотим использовать все переменные

print("PI:", pi, end=end)

import calc

print(dir(calc), end=end)

import calc.ops

print("DIV:", calc.ops.div(81,9), end=end)
