class Animal:
    weight = 200
    color = "red"
    
    def __init__(self, name): #конструктор
        print(f"Created {name}")
        self.name = name
    
    def print_weight(self, word):
        print(f"Weight {self.name}: {self.weight}")

    def __del__(self): #деструктор
        print(f"Deleted {self.name}")

a1, a2 = Animal("Murka"), Animal("Borka")
a1.weight = 300
a1.print_weight("opa")
a2.print_weight("opa")        
Animal.print_weight(a1, "opa")