class Animal:
    def __init__(self, name):
        self.name = name
    
    def sound(self):
        print("Any sound")

    def eat(self, weight):
        print(f"{self.name} eat {weight} kg of food")

class Horse(Animal):
    def sound(self):
        print(f"{self.name}: 'Igogo!'")
      
    def run(self, distance):
        print(f"{self.name} ran for {distance} meters")
        
class Bird(Animal):
    def sound(self):
        print(f"{self.name}: 'Chirik!'")
      
    def fly(self, distance):
        print(f"{self.name} flied for {distance} meters")

class Pegas(Bird, Horse):
    def magic(self):
        print(f"{self.name} make magic")       

pegas = Pegas("Pegas")
pegas.sound()
pegas.eat(20)
pegas.run(1200)
pegas.fly(300)
pegas.eat(30)