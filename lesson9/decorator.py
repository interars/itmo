def decorator(function):
    def wrapper(*args, **kwargs):
        print("ARGS:", args)
        print("KWARGS:", kwargs)
        return function(*args, **kwargs)
        
    return wrapper
    
def function(a,b):
    return a**b
    
function = decorator(function)
result = function(4,5)
print("result:",result)